from django.urls import path
from . import views

urlpatterns = [
    path('<str:resource>/', views.get_resources),
    path('<str:resource>', views.get_resources),
]
