from django.shortcuts import get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from . import models as m


form = """
        <!DOCTYPE html>
        <html lang="en">
          <body>
            <p></p>
            <form action="" method="POST">
              This is the resource: <h2>"{resource}"</h2> <br>
              Introduce a value to update it: <input name="valor" type="text" />
              <input type="submit" value="Submit" />
              <br><h3>Actual content: {value}</h3>
            </form>
          </body>
        </html>
        """


@csrf_exempt  # this means --> skip the security controls (we do it for now)
def get_resources(request, resource):
    if request.method == "PUT":
        value = request.body.decode('utf-8')  # We save the PUT body.
    elif request.method == "POST":
        value = request.POST['valor']  # We save the form content.

    # Manage the values in the database.
    if request.method in ["POST", "PUT"]:
        try:
            c = m.Content.objects.get(clave=resource)  # Search for it, in case that exist.
            c.valor = value  # We set the new value to our object Content()
        except m.Content.DoesNotExist:
            c = m.Content(clave=resource, valor=value)  # If it does not exist, you create it.
        c.save()

    # request.method == 'GET':
    try:
        content = m.Content.objects.get(clave=resource)
        answer = HttpResponse(form.format(resource=resource, value=content.valor))
    except m.Content.DoesNotExist:
        content = ""
        answer = HttpResponse('The resource does not exist' + form.format(resource=resource, value=content))

    return answer



